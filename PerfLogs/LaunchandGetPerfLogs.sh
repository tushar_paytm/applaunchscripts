#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
counter=$1
build_Name=$2
FILE_PATH=""

reg='^[0-9]+$'
if ! [[ $counter =~ $reg ]]; then
  echo "NOT NUMBER"
  FILE_PATH=$1
else
  echo "NUMBER"
  printf "Counter = ${counter}\n"
  start=0
  adb shell am force-stop net.one97.paytm
  adb shell rm -f /sdcard/Android/data/net.one97.paytm/cache/*.txt
  while [[ ${start} -ne ${counter} ]];
  do
    sleep 5
    printf "\033[01;93mCounter : $((${start}+1))\033[00m\n"
    adb shell am start -W -S net.one97.paytm/net.one97.paytm.landingpage.activity.AJRMainActivity
    sleep 5
    adb shell am force-stop net.one97.paytm
    start="$((${start}+1))"
  done

  # FileName=$(adb shell ls -Ral /sdcard/Android/data/net.one97.paytm/ | grep -i 'Paytm_PerfLogs' | awk '{print $NF}')
  # adb pull /sdcard/Android/data/net.one97.paytm/${FileName}
  FileName_OLD="$(adb shell ls -Ral /sdcard/Android/data/net.one97.paytm/cache/ | grep -i 'PaytmPerfLogs' | awk '{print $NF}')"
  file_wo_ext="${FileName_OLD%.*}"
  FileName="${file_wo_ext}_${build_Name}.txt"
  adb pull /sdcard/Android/data/net.one97.paytm/cache/"${FileName_OLD}" "${SCRIPT_DIR}/${FileName}"
  printf "\033[01;32mFile copied to : $(pwd) with Filename \"${FileName}\"\033[00m\n"
  FILE_PATH=${SCRIPT_DIR}/${FileName}
fi

###############################################################################################################################################################################################

# CJRJarvisApplication_appCreateToWindowFocusTimingLogger: end,
# setIntent(splashIntent)
# initCrashlytics()
# PaytmUpiSdk.init()
# isDeviceSecure
array_list=( "appClickToWindowFocusLogger_CJRJarvisApplication" "app_click_home_render_trace: end," "AJRMainActivity_handleOnCreate: end," "app_click_scan_anyqr_trace: end,")
array_list_new=() # Defining empty array

create_csv(){
  file=${3%:} #To remove extra ":" if exists.
  array_list_new+=(${file})
  declare -a argAry1=("${!1}")
  array_received="${argAry1[@]}"
  IFS=', ' read -r -a array <<< "${array_received}"
  printf "\033[4m\033[3mCreating \033[1m${file}.csv\033[00m\033[4m\033[3m file for trace\033[00m \"\033[1m${3}\033[00m\"\n"
  echo ${file} > ./${file}.csv
  for time in "${array[@]}"
  do
    echo "${time}" >> ./${file}.csv
  done
  printf "\033[01;32m"${file}".csv file created\033[00m\n"
}

final_output=""

for ((i = 0; i < ${#array_list[@]}; ++i)); do
  # printf "${array_list[$i]} "
  trace="$(echo ${array_list[$i]/()/\\(\\)})"
  printf "Trace = ${trace}\n"
  position=$(( $i + 1 ))
  # awk '{if($NF=="new_HomeCommunicator\(\)") print $(NF-2)$(NF-1) }' /Users/tusharrastogi/Desktop/logs/Paytm_PerfLogs_26-Aug-2020_BaseBuild_Redmi.txt | cut -d, -f1
  # new_HomeCommunicator_array=( $( awk '{if($NF=="NetworkModule.init\(\)") print $(NF-2)$(NF-1) }' /Users/tusharrastogi/Desktop/logs/Paytm_PerfLogs_26-Aug-2020_BaseBuild_Redmi.txt | cut -d, -f1) )

  #####
  if [[ "${trace}" == *"end"* ]]; then
    trace=$(echo ${trace} | cut -d' ' -f1)
    printf "${trace}\n"
    get_values_array=( $( awk -v val=${trace} '{
      if($1 == val && $2 =="end,"){
        print $(NF-1)$(NF)
      }
    }' "${FILE_PATH}"))
  else
    get_values_array=( $( awk -v val=${trace} '{if($NF==val) print $(NF-2)$(NF-1) }' "${FILE_PATH}" | cut -d, -f1))
    # get_values_array=( $( awk -v val=${new_Val} '{if($NF==val) print $(NF-2)$(NF-1) }' "${FILE_PATH}" | cut -d, -f1) )
  fi
  #####

  create_csv get_values_array[@] ${position} ${array_list[$i]}
  final_output="${final_output} ${SCRIPT_DIR}/${file}.csv"
done

if [ -z "${build_Name}" ]
then
  build_Name="final"
  printf "\033[4m\033[3mCreating final csv file with name\033[00m : \033[1m${build_Name}.csv\033[00m\n"
else
  printf "\033[4m\033[3mCreating final csv file with name\033[00m : \033[1m${build_Name}.csv\033[00m\n"
fi

printf "Merging...\n"

paste -d '\,' ${final_output} > "${build_Name}.csv"

printf "Merged\n"

for files in ${array_list_new[@]}
do
  rm -f "${SCRIPT_DIR}/${files}.csv"
done

#TODO
#FOrce Install Redmi devices
#Append device name and date
