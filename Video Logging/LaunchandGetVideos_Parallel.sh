#!/bin/bash

########

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
counter=$1
recording_time=$2
FILE_PATH=""

####################################
FILE='/Users/tusharrastogi/Desktop/Utility/Video Logging/Nokia 6.1_Base/Nokia 6.1.txt'


# S. No,ActivityTaskManager,Displayed_Time,Fully_Drawn
# 1,17:29:00.447,2ms,6s800ms
# 2,17:29:00.447,12ms,6s800ms
# 3,17:29:00.447,122ms,6s800ms
# 4,17:29:00.447,1s122ms,6s800ms
# 5,17:29:00.447,12s122ms,6s800ms
count=1
awk 'NR==1 { print "S. No,ActivityTaskManager_START,Displayed_Time,Fully_Drawn"}{
  if($6 == "ActivityTaskManager:" || $6 == "ActivityManager:")
  {
    if($7 == "START"){
      amstart = ""sprintf("%s",$2)
    }
  else if($7 == "Displayed"){
    amdisplayed = sprintf("%s",$NF)
    if (index(amdisplayed, "+"))
    {
      split(amdisplayed,arrayD,"+");
      amdisplayed = arrayD[2]
    }
    amdisplayed_Sindex =  index(amdisplayed,"s")
    amdisplayed_length = length(amdisplayed)
    if(amdisplayed_Sindex == amdisplayed_length){
      split(amdisplayed,arrayM,"m")
      amdisplayed = arrayM[1]
    }else{
      split(amdisplayed,arrayS,"s")
      seconds = arrayS[1]
      split(arrayS[2],arrayM,"m")
      milliseconds = arrayM[1]
      amdisplayed =  (seconds*1000) + milliseconds
    }
  }
else if($7 == "Fully"){
  amfully = sprintf("%s",$NF)
  if (index(amfully, "+")){
    split(amfully,arrayF,"+");
    amfully = arrayF[2]
  }

  amfully_Sindex =  index(amfully,"s")
  amfully_length = length(amfully)
  if(amfully_Sindex == amfully_length){
    split(amfully,arrayM,"m")
    amfully = arrayM[1]
  }else{
    split(amfully,arrayS,"s")
    seconds = arrayS[1]
    split(arrayS[2],arrayM,"m")
    milliseconds = arrayM[1]
    amfully =  (seconds*1000) + milliseconds
  }
  printf("%s,%s,%s,%s\n",++count,amstart,amdisplayed,amfully)
}
}
}' "${FILE}" > "/Users/tusharrastogi/Desktop/Utility/Video Logging/test.csv"

exit

####################################





if [[ $3 == "" ]]; then
  Build_Type="Undefined"
else
  Build_Type=$3
fi

printf "${Build_Type}\n"


app_name=""
app_Version(){
  if [[ -d $ANDROID_HOME ]];
  then
    echo >&2 "ANDROID_HOME variable is set"
    echo >&2 "Setting app name on the basis of App version"
    # Below ls command gets the latest directory in provided folder
    BUILDTOOL_version=$(ls -td -- $ANDROID_HOME/build-tools/*/ | head -n 1)
    aapt_file=$BUILDTOOL_version\aapt
    # echo $aapt_file
    app_stamp=$(adb -s ${1} shell dumpsys package net.one97.paytm | grep versionName | awk '{print $NF}' | awk -F '=' '{print $2}')
    app_version=$(adb -s ${1} shell dumpsys package net.one97.paytm | grep versionCode | awk '{print $1}' | awk -F '=' '{print $2}')
    app_name=$app_stamp\_$app_version
  else
    echo >&2 "ANDROID_HOME variable not set"
    app_name="app_Version"
  fi
  echo "${app_name}"
}


getDeviceinfo(){
  DEVICE_NAME=$(adb -s ${1} shell getprop ro.product.model)
  echo "${DEVICE_NAME}"
}

run_Parallel(){
  #Check if folder exists, if not create one.
  DEVICE_ID=$1
  DEVICE_NAME=$( getDeviceinfo ${1} )
  echo "Device Name = ${DEVICE_NAME}"
  app_name=$( app_Version ${1} )
  echo "App Name = ${app_name}"
}

adb devices | while read line
do
  if [ ! "$line" = "" ] && [ `echo $line | awk '{print $2}'` = "device" ]
  then
    device=`echo $line | awk '{print $1}'`
    printf "Device ID = ${device}\n"
    run_Parallel ${device} &
  fi
done

# adb shell rm -rv /sdcard/Android/data/net.one97.paytm/cache/*.txt
# awk '/appClickScanAnyQRLogger_CJRJarvisApplication/ {print $NF}'
#adb logcat ActivityManager:V | grep 'Displayed net.one97.paytm\|Fully drawn net.one97.paytm'
#zip -x "*.zip" ".*" -r "archive_$(date +"%Y-%m-%d_%H-%M-%S").zip" .

# adb shell svc power stayon true
# adb shell settings put global stay_on_while_plugged_in 3
# adb shell settings put system screen_off_timeout 60000 // 60000 = 1 minnute
# adb shell settings get system screen_off_timeout
