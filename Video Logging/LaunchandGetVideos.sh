#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
counter=$1
recording_time=$2
FILE_PATH=""

if [[ $3 == "" ]]; then
  Build_Type="Undefined"
else
  Build_Type=$3
fi

printf "${Build_Type}\n"

DEVICE_NAME=$(adb shell getprop ro.product.model)
app_name=""
app_Version(){
  if [[ -d $ANDROID_HOME ]];
  then
    echo "ANDROID_HOME variable is set"
    echo "Setting app name on the basis of App version"
    # Below ls command gets the latest directory in provided folder
    BUILDTOOL_version=$(ls -td -- $ANDROID_HOME/build-tools/*/ | head -n 1)
    aapt_file=$BUILDTOOL_version\aapt
    # echo $aapt_file
    app_stamp=$(adb shell dumpsys package net.one97.paytm | grep versionName | awk '{print $NF}' | awk -F '=' '{print $2}')
    app_version=$(adb shell dumpsys package net.one97.paytm | grep versionCode | awk '{print $1}' | awk -F '=' '{print $2}')
    app_name=$app_stamp\_$app_version
  else
    echo "ANDROID_HOME variable not set"
    app_name="app_Version"
  fi
}

if [[ ${DEVICE_NAME} == "" ]]; then
  printf "No Device detected\n"
  exit
else
  printf "Device detected : ${DEVICE_NAME}\n"
  app_Version
fi

run_Parallel(){
  #Check if folder exists, if not create one.
  DEVICE_ID=$1
  adb -s ${DEVICE_ID} shell svc power stayon true
  adb shell rm -f /sdcard/Android/data/net.one97.paytm/cache/*.txt
  if [[ -d "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}" ]]; then
    printf "Device Video Folder exists.\n"
    printf "Cleaning Folder...\n"
    #  rm -v "${SCRIPT_DIR}/${DEVICE_NAME}"/*
    cd "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}"
    find . -type f -not -name '*.zip' -print0 | xargs -0 rm --
    cd -
    printf "Folder Cleared.\n"
  else
    printf "Device Video Folder doesn't exists.\n"
    printf "Creating Device specific folder to store videos...\n"
    mkdir "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}"
    printf "Folder \"${DEVICE_NAME}_${Build_Type}\" created.\n"
  fi

  printf "Counter = ${counter}\n"
  start=0
  adb -s ${DEVICE_ID} shell am force-stop net.one97.paytm
  adb -s ${DEVICE_ID} logcat -c
  # adb -s $DEVICE_ID logcat ActivityManager:V | grep --line-buffered 'Displayed net.one97.paytm\|Fully drawn net.one97.paytm' > "${SCRIPT_DIR}/${DEVICE_NAME}/${DEVICE_NAME}.txt" &
  adb -s ${DEVICE_ID} logcat -c
  echo "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.txt"
  adb -s ${DEVICE_ID} logcat -s ActivityManager:V ActivityTaskManager:V > "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.txt" &
  # adb -s ${DEVICE_ID} logcat | grep --line-buffered 'net.one97.paytm'
  LOGPID=$!
  while [[ ${start} -ne ${counter} ]];
  do
    sleep 5
    printf "\033[01;93mCounter : $((${start}+1))\033[00m\n"
    adb -s $DEVICE_ID shell screenrecord --bugreport /sdcard/launch$((${start}+1)).mp4 &
    PID=$!
    sleep 0.7
    adb -s $DEVICE_ID shell am start -W -S net.one97.paytm/net.one97.paytm.landingpage.activity.AJRMainActivity
    sleep $((${recording_time}))
    adb -s $DEVICE_ID shell am force-stop net.one97.paytm
    kill $PID
    start="$((${start}+1))"
    # Kill background process incase kill PID fails
    trap "kill 0" SIGINT SIGTERM EXIT
  done
  kill $LOGPID

  #Parse text file to get milliseconds from formatted time value


  #Pull all Video files
  start=0
  while [[ ${start} -ne ${counter} ]];
  do
    adb -s $DEVICE_ID pull /sdcard/launch$((${start}+1)).mp4 "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}"
    adb -s $DEVICE_ID shell rm /sdcard/launch$((${start}+1)).mp4
    start="$((${start}+1))"
  done

  echo "$(pwd)"
  zip -r "${DEVICE_NAME}_${Build_Type}/${app_name}_$(date +"%Y-%m-%d_%H-%M-%S").zip" "${DEVICE_NAME}_${Build_Type}" -x "*.zip" -x "*.DS_Store"

  # Create csv
  # awk '
  # NR==1 { print "ActivityTaskManager,Displayed_Time,Fully_Drawn_Time" }
  # /ActivityManager: START u0/               { amstart = sprintf("%s",$2) }
  # /ActivityManager: Displayed/               { amdisplayed = sprintf("%s",$NF) }
  # /ActivityManager: Fully drawn/             { printf("%s,%s,%s\n",amstart,amdisplayed,$NF) }
  # /ActivityTaskManager: START u0/            { amstart = sprintf("%s",$2) }
  # /ActivityTaskManager: Displayed/           { amdisplayed = sprintf("%s",$NF) }
  # /ActivityTaskManager: Fully drawn/         { printf("%s,%s,%s\n",amstart,amdisplayed,$NF) }
  # ' "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.txt" > "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.csv"

  count=1
  awk 'NR==1 { print "S. No,ActivityTaskManager_START,Displayed_Time,Fully_Drawn"}{
    if($6 == "ActivityTaskManager:" || $6 == "ActivityManager:")
    {
      if($7 == "START"){
        amstart = ""sprintf("%s",$2)
      }
    else if($7 == "Displayed"){
      amdisplayed = sprintf("%s",$NF)
      if (index(amdisplayed, "+"))
      {
        split(amdisplayed,arrayD,"+");
        amdisplayed = arrayD[2]
      }
      amdisplayed_Sindex =  index(amdisplayed,"s")
      amdisplayed_length = length(amdisplayed)
      if(amdisplayed_Sindex == amdisplayed_length){
        split(amdisplayed,arrayM,"m")
        amdisplayed = arrayM[1]
      }else{
        split(amdisplayed,arrayS,"s")
        seconds = arrayS[1]
        split(arrayS[2],arrayM,"m")
        milliseconds = arrayM[1]
        amdisplayed =  (seconds*1000) + milliseconds
      }
    }
  else if($7 == "Fully"){
    amfully = sprintf("%s",$NF)
    if (index(amfully, "+")){
      split(amfully,arrayF,"+");
      amfully = arrayF[2]
    }
    amfully_Sindex =  index(amfully,"s")
    amfully_length = length(amfully)
    if(amfully_Sindex == amfully_length){
      split(amfully,arrayM,"m")
      amfully = arrayM[1]
    }else{
      split(amfully,arrayS,"s")
      seconds = arrayS[1]
      split(arrayS[2],arrayM,"m")
      milliseconds = arrayM[1]
      amfully =  (seconds*1000) + milliseconds
    }
    printf("%s,%s,%s,%s\n",++count,amstart,amdisplayed,amfully)
  }
}
}' "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.txt" > "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.csv"

create_csv(){
  declare -a argAry1=("${!1}")
  array_received="${argAry1[@]}"
  IFS=', ' read -r -a array <<< "${array_received}"
  printf "\033[4m\033[3mCreating \033[1m${2}_Final.csv\033[00m\033[4m\033[3m file for device\033[00m \"\033[1m${2}\033[00m\"\n"
  echo "Scan Timestamp" > "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${2}_ScanTimestamp.csv"
  for time in "${array[@]}"
  do
    echo "${time}" >> "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${2}_ScanTimestamp.csv"
  done
  printf "\033[01;32m${2}.csv file created\033[00m\n"
}

FileName_OLD=$(adb shell ls -Ral /sdcard/Android/data/net.one97.paytm/cache | grep -i 'PaytmPerfLogs' | awk '{print $NF}')

if [[ ${FileName_OLD} == "" ]]; then
  printf "No Perf File found\n"
else
  printf "Perf File found : ${FileName_OLD}\n"
  file_wo_ext=${FileName_OLD%.*}
  FileName="${file_wo_ext}_${build_Name}.txt"
  FILE_PATH="${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${FileName}"
  adb pull "/sdcard/Android/data/net.one97.paytm/cache/${FileName_OLD}" "${FILE_PATH}"
  printf "\033[01;32mFile copied to : $(pwd) with Filename \"${FileName}\"\033[00m\n"
  get_values_array=( $( awk '/appClickScanAnyQRLogger_CJRJarvisApplication/ {print $NF}' "${FILE_PATH}") )
  create_csv get_values_array[@] "${DEVICE_NAME}"
  paste -d '\,' "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.csv" "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}_ScanTimestamp.csv" > "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}_Final.csv"
  rm -f "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}.csv"
  rm -f "${SCRIPT_DIR}/${DEVICE_NAME}_${Build_Type}/${DEVICE_NAME}_ScanTimestamp.csv"
fi

}

Device_ID=$(adb shell getprop ro.serialno)
printf "Device ID = ${Device_ID}\n"

#TODO Run parallel logic
run_Parallel ${Device_ID}
#######

# adb shell rm -rv /sdcard/Android/data/net.one97.paytm/cache/*.txt
# awk '/appClickScanAnyQRLogger_CJRJarvisApplication/ {print $NF}'
#adb logcat ActivityManager:V | grep 'Displayed net.one97.paytm\|Fully drawn net.one97.paytm'
#zip -x "*.zip" ".*" -r "archive_$(date +"%Y-%m-%d_%H-%M-%S").zip" .

# adb shell svc power stayon true
# adb shell settings put global stay_on_while_plugged_in 3
# adb shell settings get global stay_on_while_plugged_in
# adb shell settings put system screen_off_timeout 60000 // 60000 = 1 minnute
# adb shell settings get system screen_off_timeout
